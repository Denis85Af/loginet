﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loginet.Models
{
    public class Album
    {
        public string UserId { get; set; }       
        public string Title { get; set; }        
    }
}