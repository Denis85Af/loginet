﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loginet.Models
{
    public class User
    { 
        public string UserName { get; set; }
        public string Email { get; set; }
        public Address Address { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public Company Company { get; set; }
    }
}