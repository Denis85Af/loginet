﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loginet.Models
{
    public class Geo
    {
        public decimal Lng { get; set; }
        public decimal Lat { get; set; }
    }
}