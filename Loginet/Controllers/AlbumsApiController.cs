﻿using Loginet.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Loginet.Controllers
{
    [RoutePrefix("api")]
    public class AlbumsApiController : ApiController
    {
        private IWebService _webService;
        public AlbumsApiController(IWebService webService)
        {
            _webService = webService;
        }

        [HttpGet]
        [Route("users")]
        public object GetAllUsers()
        {
            return _webService.GetAllUsers();
        }
        [HttpGet]
        [Route("albums")]
        public object GetAllAlbums()
        {
            return _webService.GetAllAlbums();
        }
        [HttpGet]
        [Route("users/{userId:int}")]
        public object GetUser(string userId)
        {
            return _webService.GetUserById(userId);
        }
    }
}
