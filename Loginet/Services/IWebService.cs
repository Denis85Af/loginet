﻿using Loginet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loginet.Services
{
    public interface IWebService
    {
        List<User> GetAllUsers();
        User GetUserById(string userId);
        List<Album> GetAllAlbums();
        List<Album> GetAlbumsByUser(string userId);
        Album GetAlbumById(string albumId);
    }
}
