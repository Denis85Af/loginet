﻿using Loginet.Models;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Loginet.Services
{
    public class WebService : IWebService
    {
        private RestClient client;
        private RestRequest request;
        private string url = "http://jsonplaceholder.typicode.com/";
        public WebService()
        {
            client = new RestClient();
            request = new RestRequest();
        }        
        public List<User> GetAllUsers()
        {
            client.BaseUrl = new System.Uri(url + "users");
            var response = client.Execute<List<User>>(request);
            return response.Data;
        }
        public User GetUserById(string userId)
        {
            client.BaseUrl = new System.Uri(url + "users/" + userId);
            var response = client.Execute<User>(request);           
            return response.Data;
        }
        public List<Album> GetAllAlbums()
        {
            client.BaseUrl = new System.Uri(url + "albums");
            var response = client.Execute<List<Album>>(request);
            return response.Data;
        }
        public List<Album> GetAlbumsByUser(string userId)
        {
            client.BaseUrl = new System.Uri(url + "albums?userId=" + userId);
            var response = client.Execute<List<Album>>(request);
            return response.Data;
        }
        public Album GetAlbumById(string albumId)
        {
            client.BaseUrl = new System.Uri(url + "albums/" + albumId);
            var response = client.Execute<Album>(request);
            return response.Data;
        }
    }
}